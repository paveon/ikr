from __future__ import print_function, division

import torch
import torch.utils.data
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import torchvision.models.vgg as vgg
import matplotlib.pyplot as plt
import time
import os
import copy

data_dir = './data'
train_dir = 'train'
val_dir = 'dev'
gpu_available = torch.cuda.is_available()
# gpu_available = False

data_transforms = {
    train_dir: transforms.Compose([
        # transforms.RandomResizedCrop(size=224, scale=(0.9, 1.0)),
        transforms.Resize(size=240),
        transforms.RandomRotation(degrees=7),
        transforms.RandomCrop(size=224),
        # transforms.CenterCrop(size=224),
        transforms.ColorJitter(brightness=0.1, contrast=0, saturation=0.1, hue=0),
        # transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),


    val_dir: transforms.Compose([
        transforms.Resize(size=240),
        transforms.CenterCrop(size=224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}


image_datasets = {
    x: datasets.ImageFolder(
        os.path.join(data_dir, x),
        transform=data_transforms[x]
    )
    for x in [train_dir, val_dir]
}


dataloaders = {
    x: torch.utils.data.DataLoader(
        image_datasets[x], batch_size=4,
        shuffle=True, num_workers=0
    )
    for x in [train_dir, val_dir]
}

dataset_sizes = {
    x: len(image_datasets[x]) for x in [train_dir, val_dir]
}


def imshow(inp, title=None):
    inp = inp.numpy().transpose((1, 2, 0))
    plt.axis('off')
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)


def show_databatch(inputs, classes, class_names):
    out = torchvision.utils.make_grid(inputs)
    imshow(out, title=[class_names[x] for x in classes])


def visualize_model(model, test_data, class_names, num_images=6):
    was_training = model.training

    model.train(False)
    model.eval()

    images_so_far = 0

    with torch.no_grad():
        for i, data in enumerate(test_data):
            inputs, labels = data
            if gpu_available:
                inputs = inputs.cuda()
                labels = labels.cuda()

            size = inputs.size()[0]

            outputs = model(inputs)

            _, preds = torch.max(outputs.data, 1)
            predicted_labels = [preds[j] for j in range(inputs.size()[0])]

            print("Ground truth:")
            show_databatch(inputs.data.cpu(), labels.data.cpu(), class_names)
            print("Prediction:")
            show_databatch(inputs.data.cpu(), predicted_labels, class_names)

            del inputs, labels, outputs, preds, predicted_labels
            torch.cuda.empty_cache()

            images_so_far += size
            if images_so_far >= num_images:
                break

        model.train(mode=was_training)


def train_model(model, criterion, optimizer, num_epochs=10):
    since = time.time()
    # best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    best_models = []

    train_batches = len(dataloaders[train_dir])
    val_batches = len(dataloaders[val_dir])

    n_epochs_stop = 5
    min_val_loss = np.Inf
    epochs_no_improve = 0

    for epoch in range(num_epochs):
        print("Epoch {}/{}".format(epoch + 1, num_epochs))
        print('-' * 10)

        loss_train = 0
        loss_val = 0
        acc_train = 0
        acc_val = 0

        model.train(True)

        for i, data in enumerate(dataloaders[train_dir]):
            print("\rTraining batch {}/{}".format(i + 1, train_batches), end='', flush=True)
            inputs, labels = data
            if gpu_available:
                inputs = inputs.cuda()
                labels = labels.cuda()

            optimizer.zero_grad()
            outputs = model(inputs)

            _, preds = torch.max(outputs.data, 1)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            loss_train += loss.data.item()
            acc_train += torch.sum(preds == labels.data)

            del inputs, labels, outputs, preds
            torch.cuda.empty_cache()

        avg_loss = loss_train / dataset_sizes[train_dir]
        avg_acc = acc_train.data.item() / dataset_sizes[train_dir]

        model.train(False)
        model.eval()

        with torch.no_grad():
            for i, data in enumerate(dataloaders[val_dir]):
                print("\rValidation batch {}/{}".format(i + 1, val_batches), end='', flush=True)
                inputs, labels = data
                if gpu_available:
                    inputs = inputs.cuda()
                    labels = labels.cuda()

                optimizer.zero_grad()
                outputs = model(inputs)

                _, preds = torch.max(outputs.data, 1)
                loss = criterion(outputs, labels)

                loss_val += loss.data.item()
                acc_val += torch.sum(preds == labels.data)

                del inputs, labels, outputs, preds
                torch.cuda.empty_cache()

        avg_loss_val = loss_val / dataset_sizes[val_dir]
        avg_acc_val = acc_val.data.item() / dataset_sizes[val_dir]

        print()
        print("Epoch {} result: ".format(epoch))
        print("Avg loss (train): {:.4f}".format(avg_loss))
        print("Avg acc (train): {:.4f}".format(avg_acc))
        print("Avg loss (val): {:.4f}".format(avg_loss_val))
        print("Avg acc (val): {:.4f}".format(avg_acc_val))
        print('-' * 10)
        print()

        # if avg_acc_val > best_acc:
        #     best_acc = avg_acc_val
        #     best_model_wts = copy.deepcopy(model.state_dict())

        if avg_loss_val < min_val_loss:
            model_copy = {}
            for k, v in model.state_dict().items():
                model_copy[k] = v.cpu()

            item = (model_copy, epoch, avg_acc_val)
            best_models.append(item)
            if len(best_models) > 3:
                old_model, _, _ = best_models.pop(0)
                del old_model

            epochs_no_improve = 0
            min_val_loss = avg_loss_val
            best_acc = avg_acc_val
        else:
            epochs_no_improve += 1
            if epochs_no_improve == n_epochs_stop:
                print("Early stop!")
                break

    elapsed_time = time.time() - since
    print()
    print("Training completed in {:.0f}m {:.0f}s".format(elapsed_time // 60, elapsed_time % 60))
    print("Best acc: {:.4f}".format(best_acc))

    for stored_model, epoch, acc in best_models:
        filename = "vgg16_{0}_{1}.pth".format(epoch, int(acc * 100))
        model.load_state_dict(stored_model)
        torch.save(model, filename)

    return model


def print_model_info(model):
    print(model.classifier)
    total_params = sum(p.numel() for p in model.parameters())
    print(f'{total_params:,} total parameters.')
    total_trainable_params = sum(
        p.numel() for p in model.parameters() if p.requires_grad)
    print(f'{total_trainable_params:,} training parameters.')


if __name__ == "__main__":
    plt.ion()

    for x in [train_dir, val_dir]:
        print("Loaded {} images under {}".format(dataset_sizes[x], x))

    class_names = image_datasets[train_dir].classes
    class_count = len(class_names)

    # Get a batch of training data
    # for inputs, classes in dataloaders[train_dir]:
    #     # inputs, classes = next(iter(dataloaders[train_dir]))
    #     show_databatch(inputs, classes, class_names)

    vgg16 = models.VGG(vgg.make_layers(vgg.cfg['D'], batch_norm=True), init_weights=False)
    for param in vgg16.parameters():
        param.requires_grad = False

    vgg16.load_state_dict(torch.load("vgg16_bn.pth"))

    prev_outputs = vgg16.classifier[6].in_features
    vgg16.classifier[6] = nn.Sequential(
        nn.Linear(prev_outputs, 512),
        nn.ReLU(),
        nn.Dropout(0.4),
        nn.Linear(512, class_count),
        nn.LogSoftmax(dim=1))

    # vgg16.classifier[6] = nn.Sequential(
    #     nn.Linear(prev_outputs, class_count),
    #     nn.LogSoftmax(dim=1))

    for param in vgg16.features.parameters():
        param.requires_grad = True

    print_model_info(vgg16)

    if gpu_available:
        vgg16.cuda()

    criterion = nn.NLLLoss()
    # optimizer = optim.Adam(vgg16.parameters())
    # optimizer = optim.SGD(vgg16.parameters(), lr=0.001, momentum=0.9)
    optimizer = optim.SGD(vgg16.parameters(), lr=1e-3, momentum=0.9)
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=6, gamma=0.1)
    vgg16 = train_model(vgg16, criterion, optimizer, num_epochs=80)
    exit()
