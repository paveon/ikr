from __future__ import print_function, division

from argparse import ArgumentParser
import torch
import torch.utils.data
from torchvision import datasets, transforms
import torchvision.datasets.folder as folder
import torch.nn as nn
import os

class_count = 31

data_transforms = transforms.Compose([
        transforms.Resize(size=240),
        transforms.CenterCrop(size=224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

dataset = None
dataloader = None


def eval_model(model, criterion):
    output_data = {}
    total_loss = 0
    total_correct = 0
    test_batches = len(dataloader)
    with torch.no_grad():
        model.train(False)
        model.eval()
        for i, data in enumerate(dataloader):
            print("\rEvaluating file {}/{}".format(i + 1, test_batches), end='', flush=True)

            if compare_labels:
                inputs, labels = data
            else:
                inputs = data

            if gpu_available:
                inputs = inputs.cuda()
                if compare_labels:
                    labels = labels.cuda()

            outputs = model(inputs)

            _, prediction = torch.max(outputs.data, 1)

            prob, label = torch.topk(outputs, class_count)
            for fileIdx in range(prob.size(dim=0)):
                file_results = {}
                for index in range(prob.size(dim=1)):
                    class_label = label.data[fileIdx][index].data.item()
                    class_log_prob = prob.data[fileIdx][index].data.item()
                    file_results[class_label] = class_log_prob

                img_path = dataset.samples[i*4 + fileIdx]
                if compare_labels:
                    img_path = img_path[0]

                filename = os.path.splitext(os.path.basename(img_path))[0]
                output_data[filename] = (prediction.data[fileIdx], file_results)

            if compare_labels:
                loss = criterion(outputs, labels)
                total_loss += loss.data.item()
                total_correct += torch.sum(prediction == labels)

    if compare_labels:
        avg_loss = total_loss / len(dataset)
        avg_accuracy = total_correct.data.item() / len(dataset)
        print("\nAverage NLLLoss: {:.4f}".format(avg_loss))
        print("Average accuracy: {:.4f}".format(avg_accuracy))
        print('-' * 10)
    return output_data


def make_dataset(dir, extensions):
    images = []
    dir = os.path.expanduser(dir)
    for root, _, fnames in sorted(os.walk(dir)):
        for fname in sorted(fnames):
            if folder.has_file_allowed_extension(fname, extensions):
                path = os.path.join(root, fname)
                images.append(path)

    return images


class CustomDataset(folder.data.Dataset):
    def __init__(self, root, transform=None):
        samples = make_dataset(root, folder.IMG_EXTENSIONS)
        if len(samples) == 0:
            raise(RuntimeError("Found 0 files in: " + root + "\n"
                               "Supported extensions are: " + ",".join(folder.IMG_EXTENSIONS)))

        self.root = root
        self.loader = folder.default_loader
        self.extensions = folder.IMG_EXTENSIONS

        self.samples = samples
        self.targets = [s[1] for s in samples]

        self.transform = transform

    def __getitem__(self, index):
        path = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)

        return sample

    def __len__(self):
        return len(self.samples)


if __name__ == "__main__":
    parser = ArgumentParser(add_help=False)
    parser.add_argument('-m', '--model_path', type=str)
    parser.add_argument('-d', '--data_path', type=str)
    parser.add_argument('-l', '--labels', action='store_true')
    argv = parser.parse_args()

    model_path = argv.model_path
    data_path = argv.data_path
    compare_labels = argv.labels

    if compare_labels:
        dataset = datasets.ImageFolder(data_path, transform=data_transforms)
    else:
        dataset = CustomDataset(data_path, data_transforms)

    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=4,
        shuffle=False, num_workers=0
    )

    gpu_available = torch.cuda.is_available()

    vgg16 = torch.load(model_path)

    if gpu_available:
        vgg16.cuda()

    data = eval_model(vgg16, nn.NLLLoss())

    with open('results.txt', 'w') as out:
        for key, value in data.items():
            prediction, probabilities = value
            out.write('{0} {1} '.format(key, prediction + 1))
            for i in range(class_count):
                out.write('{0} '.format(probabilities[i]))
            out.write('\n')

    exit()
